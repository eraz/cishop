RTA =

release: build/release/build.ninja
	@cd build/release; ninja -v src/libmain.so

debug: build/debug/build.ninja
	@cd build/debug; ninja -v src/libmain.so

coverage: build/coverage/build.ninja
	@lcov -q -z --directory ./build/coverage

dep:
	@git submodule update --init --recursive

run: release
	@cd build/release; ninja -v main
	cd build/release; ./main $(RTA)

check: debug
	@cd build/debug; ninja -v main
	cd build/debug; valgrind -q --leak-check=full ./main $(RTA)

test: debug
	@cd build/debug; ninja -v test/unit_tests
	cd build/debug; ./test/unit_tests --use-colour no

cover: coverage
	@cd build/coverage; ninja -v test/unit_tests
	cd build/coverage; ./test/unit_tests --use-colour no
	lcov -q -c --directory ./build/coverage --output-file ./build/coverage/meson-logs/coverage.info
	lcov -q --remove ./build/coverage/meson-logs/coverage.info "/usr/*" "*/dep/*" --output-file ./build/coverage/meson-logs/coverage.info
	lcov -l ./build/coverage/meson-logs/coverage.info

clean:
	rm -rf build

build/debug/build.ninja:
	@mkdir -p build/debug
	meson --buildtype=debug build/debug

build/release/build.ninja:
	@mkdir -p build/release
	meson --buildtype=release build/release

build/coverage/build.ninja:
	@mkdir -p build/coverage
	meson -Db_coverage=true build/coverage

.PHONY: all release debug dep run check test clean
.SUFFIXES:
