# *cishop*

---

[![][master-badge]][master-branch]

*cishop* is an experimental project exploring the configuration and usage of
    continuous integration services.

[master-badge]: <https://semaphoreci.com/api/v1/eraz/cishop/branches/master/badge.svg>
[master-branch]: <https://semaphoreci.com/eraz/cishop>
