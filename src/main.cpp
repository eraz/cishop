#include <iostream>

template <typename T>
void f() { std::cout << __PRETTY_FUNCTION__ << std::endl; }

int main() {
    f<std::string>();
}
