#ifndef A_H
#define A_H

#include <type_traits>

void a(bool);
void b();

template <typename T>
T f(T const& t) {
    if (std::is_same_v<bool, T>) {
        a(t);
        return t;
    }
    else {
        b();
        return t;
    }
}

#endif
